#!/bin/bash
# Copyright (C), 2015-2023, Sunny OIT. CO., Ltd.
# Filename: test_get_count.sh
# Author: yanjun (zngyanj@sunnyoptical.com)
# Date: 2023/01/14
# Description: 分析日志统计测试次数
# Usage: ./test_get_count.sh --help

TEST_MATCH_STRING="Start video stream Successfully"
LOG_LEVEL=1

function log_info(){
  content="[INFO]: $(date '+%Y-%m-%d %H:%M:%S') $@"
  [ $LOG_LEVEL -le 1  ] && echo -e "\033[32m"  "${content}" "\033[0m"
}

function log_warn(){
  content="[WARN]: $(date '+%Y-%m-%d %H:%M:%S') $@"
  [ $LOG_LEVEL -le 2  ] && echo -e "\033[33m"  "${content}" "\033[0m"
}

function log_err(){
  content="[ERR]: $(date '+%Y-%m-%d %H:%M:%S') $@"
  [ $LOG_LEVEL -le 3  ] && echo -e "\033[31m"  "${content}" "\033[0m"
}

function usage(){
	echo "Usage: $0 [LOG_PATH] [OPTION]"
	echo "-h, --help	show usage"
	echo "-m, --match	set match string"
	echo "-l, --loop	loop to watch"
	exit 0
}

args=("$@")
nargs="$#"
[ "$nargs" -lt 1 ] && log_err "Must specify log path!" && usage $0
[ "$nargs" -gt 3 ] && log_err "Too much options !" && usage $0
[ "${args[0]}" == '-h' -o "${args[0]}" == "--help" ] && usage $0

ARGS=$(getopt -o hlm:: --long help,loop,match:: -n "$0" -- "$@")

eval set -- "${ARGS}"

while true
do
        case $1 in
                "-m"|"--match")
                        TEST_MATCH_STRING="$2"
                        shift 2
                ;;
                "-h"|"--help")
                        usage $0
                ;;
                "-l"|"--loop")
                        loop_flag=true
			shift 1
                ;;
                --)
                        shift
                        break
                ;;
                *)
                        log_err "Unkown option $arg !"
                        exit -1
                ;;
        esac
done


LOG_PATH=$1
[ ! -f "$LOG_PATH" ] && log_err "$1 is not exist or not file !" && usage $0

function printResult(){
  log_info "log match: $TEST_MATCH_STRING"
  first_log=$(grep "$TEST_MATCH_STRING" "$LOG_PATH" | head -n 1)
  log_info "log begin: ${first_log::25}"
  last_log=$(grep "$TEST_MATCH_STRING" "$LOG_PATH" | tail -n 1)
  log_info "log end  : ${last_log::25}"
  test_cnt=$(grep "$TEST_MATCH_STRING" "$LOG_PATH" | wc -l)
  log_info "log path : $LOG_PATH"
  log_info "log count: $test_cnt"
}

function main(){
  if [ -n "$loop_flag" ];then
    log_info Loop Mode ...
    watch -c $0 "$@"
  else
    printResult
  fi
}

main $1
